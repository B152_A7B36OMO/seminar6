package seminar6;

public interface IMatrixStorage {
    public int getNumberOfRows();
    public int getLengthOfRow();
    public int[] getRow(int index);
    public int getValue(int x, int y);
    public void setValue(int x, int y, int value);
}
